// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyAI.h"
#include "EnemyCharacter.h"






AEnemyAI::AEnemyAI()
{

	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));//initialize the behavior tree

	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));//initialize the blackboard components
}

void AEnemyAI::Possess(APawn* Pawn)
{
	Super::Possess(Pawn);



	AEnemyCharacter* EnemyCharacter = Cast<AEnemyCharacter>(Pawn);
	if (EnemyCharacter)//Gets the possessed Pawn if it's the AI Character
	{
		if (EnemyCharacter->BehaviorTree->BlackboardAsset)//initialize blackboard and start behavior tree
		{
			BlackboardComp->InitializeBlackboard(*(EnemyCharacter->BehaviorTree->BlackboardAsset));
			BehaviorComp->StartTree(*EnemyCharacter->BehaviorTree);
		}
	}
}

void AEnemyAI::SetSeenTarget(APawn* Pawn)
{

	if (BlackboardComp)//Registers the Pawn in the blackboard if seen 
	{
		BlackboardComp->SetValueAsObject(BlackboardKey, Pawn);
	}
}
