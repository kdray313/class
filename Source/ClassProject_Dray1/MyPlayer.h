// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RepActor.h"
#include "Components/TextRenderComponent.h"//need that in order to use the TextRenderComponent
#include "HealthPickup.h"//calls on heathpickup we made
#include "ClassProject_Dray1GameMode.h"//calls on gamemode for the hud
#include "Engine.h"//needed for the code such as ActorItr in MyPlayer.cpp 
#include "CameraDirector.h"//needed to call on code for our camera we made
#include "GameFramework/Character.h"
#include "Net/UnrealNetwork.h"
#include "MyPlayer.generated.h"



UCLASS()
class CLASSPROJECT_DRAY1_API AMyPlayer : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

protected:

	/** The health of the character */
	UPROPERTY(VisibleAnywhere, Transient, ReplicatedUsing = OnRep_Health, Category = Stats)
		float Health;

	/** The max health of the character */
	UPROPERTY(EditAnywhere, Category = Stats)
		float MaxHealth = 100.f;

	/** The number of bombs that the character carries */
	UPROPERTY(VisibleAnywhere, Transient, ReplicatedUsing = OnRep_BombCount, Category = Stats)
		int32 BombCount;

	/** The max number of bombs that a character can have */
	UPROPERTY(EditAnywhere, Category = Stats)
		int32 MaxBombCount = 3;

	/** Text render component - used instead of UMG, to keep the tutorial short */
	UPROPERTY(VisibleAnywhere)
		UTextRenderComponent* CharText;

private:
	/**
	* TakeDamage Server version. Call this instead of TakeDamage when you're a client
	* You don't have to generate an implementation. It will automatically call the ServerTakeDamage_Implementation function
	*/
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerTakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

	/** Contains the actual implementation of the ServerTakeDamage function */
	void ServerTakeDamage_Implementation(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

	/** Validates the client. If the result is false the client will be disconnected */
	bool ServerTakeDamage_Validate(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

	//Bomb related functions

	/** Will try to spawn a bomb */
	void AttempToSpawnBomb();

	/** Returns true if we can throw a bomb */
	bool HasBombs() { return BombCount > 0; }

	/**
	* Spawns a bomb. Call this function when you're authorized to.
	* In case you're not authorized, use the ServerSpawnBomb function.
	*/
	void SpawnBomb();

	/**
	* SpawnBomb Server version. Call this instead of SpawnBomb when you're a client
	* You don't have to generate an implementation for this. It will automatically call the ServerSpawnBomb_Implementation function
	*/
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerSpawnBomb();

	/** Contains the actual implementation of the ServerSpawnBomb function */
	void ServerSpawnBomb_Implementation();

	/** Validates the client. If the result is false the client will be disconnected */
	bool ServerSpawnBomb_Validate();

	/** Called when the Health variable gets updated */
	UFUNCTION()
		void OnRep_Health();

	/** Called when the BombCount variable gets updated */
	UFUNCTION()
		void OnRep_BombCount();

	/** Initializes health */
	void InitHealth();

	/** Initializes bomb count */
	void InitBombCount();

	/** Updates the character's text to match with the updated stats */
	void UpdateCharText();

public:

	/** Bomb Blueprint */
	UPROPERTY(EditAnywhere)
		TSubclassOf<ARepActor> BombActorBP;

	/** Applies damage to the character */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	/** Marks the properties we wish to replicate */
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// Sets default values for this character's properties
	AMyPlayer();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Power)//used to work with blueprints in unreal
		USphereComponent* CollectionSphere;	// Collection area around the character


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Power)//used to work with blueprints in unreal
		float PowerLevel;	// Power level of our character

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Power)//used to work with blueprints in unreal
		float SpeedFactor;	// Power level of our character

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Power)//used to work with blueprints in unreal
		float BaseSpeed;	// Power level of our character

		// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Power)//used to work with blueprints in unreal
		void CollectHealth();	//Called when we pressing left mouse click, to collect health




	UFUNCTION(BlueprintImplementableEvent, Category = Power)//used to work with blueprints in unreal
		void PowerUp(float HealthLevel);	//for CollectBatteries() to use Blueprinted PowerUp



public:


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

		/** Resets HMD orientation in VR. */
	void OnResetVR();

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }



	template <int Index>//to define the index for changeView 
	void ChangeView() { SetView(Index); }// Declares changeview to use the setview function to interpret the input to change the camera
	void SetView(int CamNum);// Declares function to read the input





};
