// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"
#include "Item.generated.h"

UCLASS()
class CLASSPROJECT_DRAY1_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		FRotator RotationRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		UStaticMeshComponent* PickupMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		USceneComponent* PickupRoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		float Speed;

	UPROPERTY(Editanywhere)
		UBoxComponent* BoxCollider;

	UFUNCTION()
		void OnComponentBegin(UPrimitiveComponent* OverlappedComponent, 
			AActor* OtherActor, 
			UPrimitiveComponent* OtherComp, 
			int32 OtherBodyIndex,
			bool bFromSweep, 
			const FHitResult & SweepResult);// defines function for our overlap


};
