// Fill out your copyright notice in the Description page of Project Settings.

#include "WEye1.h"

// Sets default values
AWEye1::AWEye1()
{
	ThisScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	RootComponent = ThisScene;

	ThisMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GenerateMesh"));
	ThisMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWEye1::PostActorCreated()
{
	Super::PostActorCreated();
	GenerateMesh();
}

void AWEye1::PostLoad()
{
	Super::PostLoad();
	GenerateMesh();
}


void AWEye1::GenerateMesh()
{

	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	Tangents.Reset();
	UVs.Reset();
	Colors.Reset();

	int32 TriangleIndexCount = 0;
	FVector DefineShape[8];
	FProcMeshTangent TangentSetup;

	DefineShape[0] = FVector(CubeRaduis.X, CubeRaduis.Y, CubeRaduis.Z);
	DefineShape[1] = FVector(CubeRaduis.X, CubeRaduis.Y, -CubeRaduis.Z);
	DefineShape[2] = FVector(CubeRaduis.X, -CubeRaduis.Y, CubeRaduis.Z);
	DefineShape[3] = FVector(CubeRaduis.X, -CubeRaduis.Y, -CubeRaduis.Z);

	DefineShape[4] = FVector(-CubeRaduis.X, -CubeRaduis.Y, CubeRaduis.Z);
	DefineShape[5] = FVector(-CubeRaduis.X, -CubeRaduis.Y, -CubeRaduis.Z);
	DefineShape[6] = FVector(-CubeRaduis.X, CubeRaduis.Y, CubeRaduis.Z);
	DefineShape[7] = FVector(-CubeRaduis.X, CubeRaduis.Y, -CubeRaduis.Z);




	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(DefineShape[0], DefineShape[1], DefineShape[2], DefineShape[3], TriangleIndexCount, TangentSetup);

	TangentSetup = FProcMeshTangent(1.f, 0.f, 0.f);
	AddQuadMesh(DefineShape[2], DefineShape[3], DefineShape[4], DefineShape[5], TriangleIndexCount, TangentSetup);

	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(DefineShape[4], DefineShape[5], DefineShape[6], DefineShape[7], TriangleIndexCount, TangentSetup);

	TangentSetup = FProcMeshTangent(-1.f, 0.f, 0.f);
	AddQuadMesh(DefineShape[6], DefineShape[7], DefineShape[0], DefineShape[1], TriangleIndexCount, TangentSetup);

	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(DefineShape[6], DefineShape[0], DefineShape[4], DefineShape[2], TriangleIndexCount, TangentSetup);

	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(DefineShape[1], DefineShape[7], DefineShape[3], DefineShape[5], TriangleIndexCount, TangentSetup);

	ThisMesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);

}

void AWEye1::AddTriangleMesh(FVector TopRight, FVector BottomRight, FVector BottomLeft, int32 & TriIndex, FProcMeshTangent Tangent)
{

	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;

	Vertices.Add(TopRight);
	Vertices.Add(BottomRight);
	Vertices.Add(BottomLeft);

	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	FVector ThisNorm = FVector::CrossProduct(TopRight, BottomRight).GetSafeNormal();
	for (int i = 0; i < 3; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Green);
	}

	UVs.Add(FVector2D(0.f, 1.f));
	UVs.Add(FVector2D(0.f, 0.f));
	UVs.Add(FVector2D(1.f, 0.f));


}

void AWEye1::AddQuadMesh(FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32 & TriIndex, FProcMeshTangent Tangent)
{
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;
	int32 Point4 = TriIndex++;

	Vertices.Add(TopRight);
	Vertices.Add(BottomRight);
	Vertices.Add(TopLeft);
	Vertices.Add(BottomLeft);


	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	Triangles.Add(Point4);
	Triangles.Add(Point3);
	Triangles.Add(Point2);

	FVector ThisNorm = FVector::CrossProduct(TopLeft - BottomRight, TopLeft - TopRight).GetSafeNormal();
	for (int i = 0; i < 4; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Green);
	}

	UVs.Add(FVector2D(0.f, 1.f));
	UVs.Add(FVector2D(0.f, 0.f));
	UVs.Add(FVector2D(1.f, 1.f));
	UVs.Add(FVector2D(1.f, 0.f));


}