// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAI.generated.h"

/**
*
*/
UCLASS()
class CLASSPROJECT_DRAY1_API AEnemyAI : public AAIController
{
	GENERATED_BODY()

private:

	UBehaviorTreeComponent* BehaviorComp;//Behavior Tree reference


	UBlackboardComponent* BlackboardComp;//Blackboard comp reference

public:


	AEnemyAI();//constructor


	UPROPERTY(EditDefaultsOnly, Category = "AI")
		FName BlackboardKey = "Target";//Blackboard key set up in Unreal with the name Target


	virtual void Possess(APawn* Pawn) override;//Executes when the controller possess a Pawn


	void SetSeenTarget(APawn* Pawn);//Sets the target after it's seen

};
