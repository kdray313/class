// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayer.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "kismet/GameplayStatics.h"



// Sets default values
AMyPlayer::AMyPlayer()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PowerLevel = 2000.f;	//set starting power level for the character

	//set the dependence of speed on the power level
	SpeedFactor = 0.75f;
	BaseSpeed = 10.0f;

	//Create our health collection volume
	CollectionSphere = CreateDefaultSubobject<USphereComponent>("CollectionSphere");
	CollectionSphere->AttachTo(RootComponent);
	CollectionSphere->SetSphereRadius(200.f);

	//Init. text render comp
	CharText = CreateDefaultSubobject<UTextRenderComponent>(FName("CharText"));

	//Set a relative location
	CharText->SetRelativeLocation(FVector(0, 0, 100));

	//Attach it to root comp
	CharText->SetupAttachment(GetRootComponent());

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

}


void AMyPlayer::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Tell the engine to call the OnRep_Health and OnRep_BombCount each time
	//a variable changes
	DOREPLIFETIME(AMyPlayer, Health);
	DOREPLIFETIME(AMyPlayer, BombCount);
}

void AMyPlayer::OnRep_Health()
{
	UpdateCharText();
}

void AMyPlayer::OnRep_BombCount()
{
	UpdateCharText();
}

void AMyPlayer::InitHealth()
{
	Health = MaxHealth;
	UpdateCharText();
}

void AMyPlayer::InitBombCount()
{
	BombCount = MaxBombCount;
	UpdateCharText();
}

// Called when the game starts or when spawned
void AMyPlayer::BeginPlay()
{
	Super::BeginPlay();

	InitHealth();
	InitBombCount();
}

void AMyPlayer::UpdateCharText()
{
	//Create a string that will display the health and bomb count values
	FString NewText = FString("Health: ") + FString::SanitizeFloat(Health) + FString(" Bomb Count: ") + FString::FromInt(BombCount);

	//Set the created string to the text render comp
	CharText->SetText(FText::FromString(NewText));
}


float AMyPlayer::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	//Decrease the character's hp 

	Health -= Damage;
	if (Health <= 0) InitHealth();

	//Call the update text on the local client
	//OnRep_Health will be called in every other client so the character's text
	//will contain a text with the right values
	UpdateCharText();

	return Health;
}

void AMyPlayer::ServerTakeDamage_Implementation(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

bool AMyPlayer::ServerTakeDamage_Validate(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	//Assume that everything is ok without any further checks and return true
	return true;
}

void AMyPlayer::AttempToSpawnBomb()
{
	if (HasBombs())
	{
		if (Role < ROLE_Authority)
		{
			ServerSpawnBomb();
		}
		else SpawnBomb();
	}
}

void AMyPlayer::SpawnBomb()
{
	//Decrease the bomb count and update the text in the local client
	//OnRep_BombCount will be called in every other client
	BombCount--;
	UpdateCharText();

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;
	SpawnParameters.Owner = GetController();

	//Spawn the bomb
	GetWorld()->SpawnActor<AMyPlayer>(BombActorBP, GetActorLocation() + GetActorForwardVector() * 200, GetActorRotation(), SpawnParameters);
}

void AMyPlayer::ServerSpawnBomb_Implementation()
{
	SpawnBomb();
}

bool AMyPlayer::ServerSpawnBomb_Validate()
{
	return true;
}


// Called every frame
void AMyPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GetCharacterMovement()->MaxWalkSpeed = SpeedFactor * PowerLevel + BaseSpeed;//sets the player to move slower when powerlevel drops
	GetCharacterMovement()->MaxAcceleration = 600.0f;//sets the acceleration to reach max speed

}

// Called to bind functionality to input
void AMyPlayer::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{

	check(InputComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &AMyPlayer::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &AMyPlayer::StopJumping);
	InputComponent->BindAxis("MoveForward", this, &AMyPlayer::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMyPlayer::MoveRight);
	InputComponent->BindAction("Cam1", IE_Pressed, this, &AMyPlayer::ChangeView<1>);//reads input to change to camera 1
	InputComponent->BindAction("Cam2", IE_Pressed, this, &AMyPlayer::ChangeView<2>);//reads input to change to camera 2
	InputComponent->BindAction("CamF", IE_Pressed, this, &AMyPlayer::ChangeView<0>);//reads input to change to Player camera
	InputComponent->BindAction("CollectPickups", IE_Pressed, this, &AMyPlayer::CollectHealth);//reads input to collect health with mouse click
	InputComponent->BindAction("ThrowBomb", IE_Pressed, this, &AMyPlayer::AttempToSpawnBomb);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
// "turn" handles devices that provide an absolute delta, such as a mouse.
// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &AMyPlayer::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AMyPlayer::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &AMyPlayer::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AMyPlayer::LookUpAtRate);

	// handle touch devices
	InputComponent->BindTouch(IE_Pressed, this, &AMyPlayer::TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &AMyPlayer::TouchStopped);

	// VR headset functionality
	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AMyPlayer::OnResetVR);
}




void AMyPlayer::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMyPlayer::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AMyPlayer::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AMyPlayer::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMyPlayer::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}




void AMyPlayer::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AMyPlayer::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}


void AMyPlayer::SetView(int CamNum)//function that reads input for camera changes
{
	for (TActorIterator<ACameraDirector> ActorItr(GetWorld()); ActorItr; ++ActorItr)//calls on the camera Actors
	{
		if (CamNum == 0)// checks for button 0
		{
			ActorItr->SetCamF();//if button 0 Camera is set to CamF
			break;//ends the function
		}
		else if (CamNum == 1)// checks for button 1
		{
			ActorItr->SetCam1();//if button 0 Camera is set to Cam1
			break;//ends the function
		}
		else if (CamNum == 2)// checks for button 2
		{
			ActorItr->SetCam2();//if button 0 Camera is set to Cam2
			break;//ends the function
		}
	}
}

void AMyPlayer::CollectHealth()//collect health function
{
	float HealthLevel = 0.f;//sets HealthLevel

	AClassProject_Dray1GameMode* MyGameMode = Cast<AClassProject_Dray1GameMode>(UGameplayStatics::GetGameMode(this));//calls and sets game mode

	if (MyGameMode->GetCurrentState() == EClassProject_Dray1PlayState::EPlaying)	//if we are currently playing

	{

		TArray<AActor*> CollectedActors;		//Gets overlapping Actors and store them in a CollectedActors array

		CollectionSphere->GetOverlappingActors(CollectedActors);

		for (int32 iCollected = 0; iCollected < CollectedActors.Num(); ++iCollected)		//For each Actor collected

		{
			AHealthPickup* const TestHealth = Cast<AHealthPickup>(CollectedActors[iCollected]);			// Cast the collected Actor to AHealthPickup


			if (TestHealth && !TestHealth->IsPendingKill() && TestHealth->bIsActive)	//if the cast is successful, and the health is valid and active 

			{
				HealthLevel = HealthLevel + TestHealth->PowerLevel;		// Store health power for adding to the character's power

				TestHealth->OnPickedUp();	// Call the health's OnPickedUp function

				TestHealth->bIsActive = false;		// Deactivate the health


			}
		}

		if (HealthLevel > 0.f)
		{
			PowerUp(HealthLevel);	// Calls Blueprinted implementation of PowerUp with the total battery power as input

		}
	}
}
