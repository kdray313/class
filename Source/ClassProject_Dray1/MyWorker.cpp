// Fill out your copyright notice in the Description page of Project Settings.

#include "MyWorker.h"


// Sets default values
AMyWorker::AMyWorker()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyWorker::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyWorker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyWorker::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

