// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"//needed for the code such as ActorItr in MyPlayer.cpp 
#include "CameraDirector.h"//needed to call on code for our camera we made
#include "GameFramework/Pawn.h"
#include "ComponentPawn.generated.h"

UCLASS()
class CLASSPROJECT_DRAY1_API AComponentPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AComponentPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UParticleSystemComponent* OurParticleSystem;//keeps track of a Particle System Component
	class UCollidingPawnMovementComponent* OurMovementComponent;// keeps track of our Pawn Movement Component

	virtual UPawnMovementComponent* GetMovementComponent() const override;// overrides the GetMovementComponent function so that it returns our custom Pawn Movement Component

	void Lateral(float AxisValue);//defines button for W & S
	void SidetoSide(float AxisValue);//defines button for A & D
	void Turn(float AxisValue);//defines Mouse movment on teh x Axis
	void ParticleToggle();//defines button for SPACEBAR
	template <int Index>//to define the index for changeView 
	void ChangeView() { SetView(Index); }// Declares changeview to use the setview function to interpret the input to change the camera
	void SetView(int CamNum);// Declares function to read the input


};
