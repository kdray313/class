// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"


// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PickupRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = PickupRoot;


	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MESH"));
	PickupMesh->AttachToComponent(PickupRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);


	RotationRate = FRotator(0.0f, 180.0f, 0.0f);

	Speed = 1.0f;

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	BoxCollider->SetGenerateOverlapEvents(true);
	BoxCollider->SetWorldScale3D(FVector(1.0f,1.0f,1.0));
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &AItem::OnComponentBegin);
	BoxCollider->AttachToComponent(PickupRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);

}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalRotation(RotationRate* DeltaTime* Speed);

}

void AItem::OnComponentBegin(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{

	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{

		FString pickup = FString::Printf(TEXT("Picked Up: %s"), *GetName());
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, pickup);
		Destroy();

	}

}



