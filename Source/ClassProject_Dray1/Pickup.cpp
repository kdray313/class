// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"


// Sets default values
APickup::APickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bIsActive = true;	//The pickup is valid when it is created


	BaseCollisionComponent = CreateDefaultSubobject<USphereComponent>("BaseSphereComponent");	// root SphereComponent to handle pickup's collision


	RootComponent = BaseCollisionComponent;	// Set SphereComponent as the root component


	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>("PickupMesh");	//Create the mesh component 


	PickupMesh->SetSimulatePhysics(true);	//Turn physics on for the mesh


	PickupMesh->AttachTo(RootComponent);	//attach the StaticMeshComponent to the root component


}

void APickup::OnPickedUp_Implementation()//no default behavior for a Pickup when it's picked up.
{
	//nothing
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

