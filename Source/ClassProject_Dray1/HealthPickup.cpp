// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthPickup.h"


AHealthPickup::AHealthPickup()
{
	PowerLevel = 150.f;	//power level of the health

}

void AHealthPickup::OnPickedUp_Implementation()
{
	Super::OnPickedUp_Implementation();	// Call the parent implementation of OnPickedUp

	Destroy();	// Destroy the health

}
