// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class CLASSPROJECT_DRAY1_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

private:
	UFUNCTION()
		void OnSeePlayer(APawn* Pawn);

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, Category = "AI")// component used to have the AI "see"
		class UPawnSensingComponent* PawnSensingComp;


	UPROPERTY(EditAnywhere, Category = "AI")//The Behavior Tree of the Character
		class UBehaviorTree* BehaviorTree;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;



};
