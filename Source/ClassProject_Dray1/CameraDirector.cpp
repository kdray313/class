// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraDirector.h"
#include "kismet/GameplayStatics.h"

// Sets default values
ACameraDirector::ACameraDirector()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
	Super::BeginPlay();

	OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);//sets OurPlayerController as the playercontroller
	CamF = OurPlayerController->GetViewTarget();//This sets that CamF is the starting view and is the player camera
}

// Called every frame
void ACameraDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ACameraDirector::SetCam1()//function to change camera to Cam1
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != Cam1)//if the camera isn't on cam1
		{
			OurPlayerController->SetViewTargetWithBlend(Cam1, SmoothBlendTime);//sets camera on cam1 with smoothblend
		}
	}
}

void ACameraDirector::SetCam2()//function to change camera to Cam2
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != Cam2)//if the camera isn't on cam2
		{
			OurPlayerController->SetViewTargetWithBlend(Cam2, SmoothBlendTime);//sets camera on cam2 with smoothblend
		}
	}
}

void ACameraDirector::SetCamF()//function to change camera to CamF(player camera)
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != CamF)//if the camera isn't on CamF(player camera)
		{
			OurPlayerController->SetViewTargetWithBlend(CamF, SmoothBlendTime);//sets camera on CamF(player camera) with smoothblend
		}
	}
}
