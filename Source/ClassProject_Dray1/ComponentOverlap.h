// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Components/SphereComponent.h" // used for sphere components
#include "CoreMinimal.h"
#include "Engine.h"// needed for the text on the screen to apear
#include "GameFramework/Actor.h"
#include "ComponentOverlap.generated.h"



UCLASS()
class CLASSPROJECT_DRAY1_API AComponentOverlap : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AComponentOverlap(const FObjectInitializer& objectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);// defines function for our overlap

private:
	USphereComponent* _collision;// defines collision


};
