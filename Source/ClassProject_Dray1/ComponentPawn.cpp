// Fill out your copyright notice in the Description page of Project Settings.

#include "ComponentPawn.h" // calls on ComponentPawn.h
#include "Camera/CameraComponent.h"  // used for camera components
#include "CollidingPawnMovementComponent.h"  // used for OurMovementComponent
#include "Components/InputComponent.h" // used for player input
#include "Components/StaticMeshComponent.h" // used for sphere mesh
#include "Components/SphereComponent.h" // used for the SphereComponent
#include "ConstructorHelpers.h" // used for getting our sphere and fire asset in our project files
#include "GameFramework/SpringArmComponent.h" // used for SpringArm
#include "Particles/ParticleSystemComponent.h" // used for ParticleSystem


// Sets default values
AComponentPawn::AComponentPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Our root component will be a sphere that reacts to physics
	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));//defines SphereComponent
	RootComponent = SphereComponent;//sets RootComponent as SphereComponent 
	SphereComponent->InitSphereRadius(40.0f);//sets the Collision Sphere radius
	SphereComponent->SetCollisionProfileName(TEXT("Pawn"));//gives the Collision Sphere the name Pawn

														   // Create and position a mesh component so we can see where our sphere is
	UStaticMeshComponent* SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));//defines SphereVisual
	SphereVisual->SetupAttachment(RootComponent);//attaches SphereVisual to RootComponent
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));//gets the sphere shape in our project files
	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);//sets the Mesh asset for the Collision Sphere
		SphereVisual->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));//set location of the Collision Sphere relative to its parent 
		SphereVisual->SetWorldScale3D(FVector(0.8f));//Set the scale of the Collision Sphere
	}

	// Create a particle system that we can activate or deactivate
	OurParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("MovementParticles"));//defines OurParticleSystem
	OurParticleSystem->SetupAttachment(SphereVisual);//attaches ParticleSystem to SphereVisual 
	OurParticleSystem->bAutoActivate = false;// turns ParticleSystem off
	OurParticleSystem->SetRelativeLocation(FVector(-20.0f, 0.0f, 20.0f));//set location of the ParticleSystem relative to its parent 
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAsset(TEXT("/Game/StarterContent/Particles/P_Fire.P_Fire"));//gets the fire in our project files
	if (ParticleAsset.Succeeded())
	{
		OurParticleSystem->SetTemplate(ParticleAsset.Object);//reveals the ParticleAsset (fire)
	}

	// Use a spring arm to give the camera smooth, natural-feeling motion.
	USpringArmComponent* SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraAttachmentArm"));//defines SpringArm
	SpringArm->SetupAttachment(RootComponent);//attaches SpringArm to RootComponent
	SpringArm->RelativeRotation = FRotator(-45.f, 0.f, 0.f);//set the camera to rotate on the x axis
	SpringArm->TargetArmLength = 400.0f;//set distance behind object
	SpringArm->bEnableCameraLag = true;// turn camera on
	SpringArm->CameraLagSpeed = 3.0f;//controls how quickly camera reaches target position

									 // Create a camera and attach to our spring arm
	UCameraComponent* Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));//defines Camera
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);//attaches SpringArm to camera 

																		// Take control of the default player
	AutoPossessPlayer = EAutoReceiveInput::Player0;


	OurMovementComponent = CreateDefaultSubobject<UCollidingPawnMovementComponent>(TEXT("CustomMovementComponent"));//Create an instance of our movement component
	OurMovementComponent->UpdatedComponent = RootComponent;//tells instance to update our root component
}

// Called when the game starts or when spawned
void AComponentPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AComponentPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AComponentPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)//function to set player input 
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAction("ParticleToggle", IE_Pressed, this, &AComponentPawn::ParticleToggle); //binds the particle to pressing spacebare

	InputComponent->BindAxis("Lateral", this, &AComponentPawn::Lateral);//binds moving forward and back to w & s
	InputComponent->BindAxis("SidetoSide", this, &AComponentPawn::SidetoSide);//binds moving side to side to a & d
	InputComponent->BindAxis("Turn", this, &AComponentPawn::Turn);//binds turning on the x axis to the the mouse 
	InputComponent->BindAction("Cam1", IE_Pressed, this, &AComponentPawn::ChangeView<1>);//reads input to change to camera 1
	InputComponent->BindAction("Cam2", IE_Pressed, this, &AComponentPawn::ChangeView<2>);//reads input to change to camera 2
	InputComponent->BindAction("CamF", IE_Pressed, this, &AComponentPawn::ChangeView<0>);//reads input to change to Player camera

}

UPawnMovementComponent* AComponentPawn::GetMovementComponent() const// defines of our overridden function
{
	return OurMovementComponent;
}

void AComponentPawn::Lateral(float AxisValue) // function that sets up moving forward and backward
{
	if (OurMovementComponent && (OurMovementComponent->UpdatedComponent == RootComponent))
	{
		OurMovementComponent->AddInputVector(GetActorForwardVector() * AxisValue);
	}
}

void AComponentPawn::SidetoSide(float AxisValue) // function that sets up moving side to side
{
	if (OurMovementComponent && (OurMovementComponent->UpdatedComponent == RootComponent))
	{
		OurMovementComponent->AddInputVector(GetActorRightVector() * AxisValue);
	}
}

void AComponentPawn::Turn(float AxisValue) // function that sets up turning on the mouse moving 
{
	FRotator NewRotation = GetActorRotation();
	NewRotation.Yaw += AxisValue;
	SetActorRotation(NewRotation);
}

void AComponentPawn::ParticleToggle()// function that sets up turning on the fire 
{
	if (OurParticleSystem && OurParticleSystem->Template)
	{
		OurParticleSystem->ToggleActive();
	}
}

void AComponentPawn::SetView(int CamNum)//function that reads input for camera changes
{
	for (TActorIterator<ACameraDirector> ActorItr(GetWorld()); ActorItr; ++ActorItr)//calls on the camera Actors
	{
		if (CamNum == 0)// checks for button 0
		{
			ActorItr->SetCamF();//if button 0 Camera is set to CamF
			break;//ends the function
		}
		else if (CamNum == 1)// checks for button 1
		{
			ActorItr->SetCam1();//if button 0 Camera is set to Cam1
			break;//ends the function
		}
		else if (CamNum == 2)// checks for button 2
		{
			ActorItr->SetCam2();//if button 0 Camera is set to Cam2
			break;//ends the function
		}
	}
}