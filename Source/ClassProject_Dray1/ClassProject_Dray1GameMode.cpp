// Fill out your copyright notice in the Description page of Project Settings.

#include "ClassProject_Dray1GameMode.h"
#include "Kismet/GameplayStatics.h"
#include "MyPlayer.h"
#include "MyHUD.h"//calls hud we made


AClassProject_Dray1GameMode::AClassProject_Dray1GameMode()
{

	HUDClass = AMyHUD::StaticClass();	//set the default HUD class


	DecayRate = 0.25f;	//base decay rate

	PrimaryActorTick.bCanEverTick = true;//allows the tick for the power level to drop 

}

void AClassProject_Dray1GameMode::Tick(float DeltaSeconds)
{
	AMyPlayer* MyCharacter = Cast<AMyPlayer>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));//sets MyPlayer to Mycharacter to impliment the power decay 

	if (MyCharacter->PowerLevel > 0.05)	//if the character still has power

	{
		MyCharacter->PowerLevel = FMath::FInterpTo(MyCharacter->PowerLevel, 0.f, DeltaSeconds, DecayRate);		//decrease the character's power

	}
	else
	{
		SetCurrentState(EClassProject_Dray1PlayState::EGameOver);// ends the game if the character power level drops below .05
	}
}

void AClassProject_Dray1GameMode::SetCurrentState(EClassProject_Dray1PlayState NewState)
{
	CurrentState = NewState;

	HandleNewState(NewState);
}

void AClassProject_Dray1GameMode::HandleNewState(EClassProject_Dray1PlayState NewState)
{
	switch (NewState)
	{
		// When we're playing, the spawn volumes can spawn
	case EClassProject_Dray1PlayState::EPlaying:
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->EnableSpawning();
		}
		break;

		// if the game is over, the spawn volumes should deactivate
	case EClassProject_Dray1PlayState::EGameOver:
	{
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->DisableSpawning();
		}
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
		PlayerController->SetCinematicMode(true, true, true);
	}
	break;

	case EClassProject_Dray1PlayState::EUnknown:// for if theres an error
	default:
		break;
	}
}

void AClassProject_Dray1GameMode::BeginPlay()
{
	Super::BeginPlay();

	//find all spawn volume actors
	TArray<AActor*> FoundActors;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnVolume::StaticClass(), FoundActors);

	for (auto Actor : FoundActors)
	{
		ASpawnVolume* SpawnVolumeActor = Cast<ASpawnVolume>(Actor);
		if (SpawnVolumeActor)
		{
			SpawnVolumeActors.Add(SpawnVolumeActor);
		}
	}


	SetCurrentState(EClassProject_Dray1PlayState::EPlaying);


}