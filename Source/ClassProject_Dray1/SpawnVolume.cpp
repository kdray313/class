// Fill out your copyright notice in the Description page of Project Settings.

#include "SpawnVolume.h"


// Sets default values
ASpawnVolume::ASpawnVolume()
{

	WhereToSpawn = CreateDefaultSubobject<UBoxComponent>("WhereToSpawn");	// Creates StaticMeshComponent for spawner


	RootComponent = WhereToSpawn;	// Set StaticMeshComponent as root component


	// Set the spawn delay range and get the first SpawnDelay
	SpawnDelayRangeLow = 1.0f;
	SpawnDelayRangeHigh = 4.5f;
	SpawnDelay = GetRandomSpawnDelay();

	PrimaryActorTick.bCanEverTick = true;	// Make the SpawnVolume tickable



}

void ASpawnVolume::SpawnPickup()
{
	if (WhatToSpawn != NULL)	// If we have set a spawn:

	{
		UWorld* const World = GetWorld();		// Check for World: 

		if (World)
		{
			// Set the spawn parameters
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;

			FVector SpawnLocation = GetRandomPointInVolume();	// random location to spawn


			// Get a random rotation for the spawned item
			FRotator SpawnRotation;
			SpawnRotation.Yaw = FMath::FRand() * 360.f;
			SpawnRotation.Pitch = FMath::FRand() * 360.f;
			SpawnRotation.Roll = FMath::FRand() * 360.f;

			APickup* const SpawnedPickup = World->SpawnActor<APickup>(WhatToSpawn, SpawnLocation, SpawnRotation, SpawnParams);	// spawn a pickup

		}
	}
}

float ASpawnVolume::GetRandomSpawnDelay()
{
	return FMath::FRandRange(SpawnDelayRangeLow, SpawnDelayRangeHigh);	// Gets random float in spawn delay range

}


FVector ASpawnVolume::GetRandomPointInVolume()
{
	FVector RandomLocation;
	float MinX, MinY, MinZ;
	float MaxX, MaxY, MaxZ;

	FVector Origin;
	FVector BoxExtent;

	Origin = WhereToSpawn->Bounds.Origin;	// Get the SpawnVolume's origin
	BoxExtent = WhereToSpawn->Bounds.BoxExtent;// Get the SpawnVolume's box extent

	// Calculate the minimum X, Y, and Z
	MinX = Origin.X - BoxExtent.X / 2.f;
	MinY = Origin.Y - BoxExtent.Y / 2.f;
	MinZ = Origin.Z - BoxExtent.Z / 2.f;

	// Calculate the maximum X, Y, and Z
	MaxX = Origin.X + BoxExtent.X / 2.f;
	MaxY = Origin.Y + BoxExtent.Y / 2.f;
	MaxZ = Origin.Z + BoxExtent.Z / 2.f;

	// The random spawn location will fall between the min and max X, Y, and Z
	RandomLocation.X = FMath::FRandRange(MinX, MaxX);
	RandomLocation.Y = FMath::FRandRange(MinY, MaxY);
	RandomLocation.Z = FMath::FRandRange(MinZ, MaxZ);

	return RandomLocation;	// Return the random spawn location

}

void ASpawnVolume::Tick(float DeltaSeconds)
{
	// If spawning is not enabled, do nothing
	if (!bSpawningEnabled)
		return;

	SpawnTime += DeltaSeconds;	//add delta time to Spawn Time


	bool bShouldSpawn = (SpawnTime > SpawnDelay);

	if (bShouldSpawn)
	{
		SpawnPickup();

		SpawnTime -= SpawnDelay;	//minus spawn delay from accumulated time


		SpawnDelay = GetRandomSpawnDelay();

	}
}

void ASpawnVolume::EnableSpawning()
{
	bSpawningEnabled = true;
}

void ASpawnVolume::DisableSpawning()
{
	bSpawningEnabled = false;
}



