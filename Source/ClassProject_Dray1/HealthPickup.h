// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "HealthPickup.generated.h"

/**
 *
 */
UCLASS()
class CLASSPROJECT_DRAY1_API AHealthPickup : public APickup
{
	GENERATED_BODY()

public:
	AHealthPickup();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Power)//used to work with blueprints in unreal
		float PowerLevel;	//amount of power the health gives


	void OnPickedUp_Implementation() override;	//Override the OnPickedUp function (use Implementation because this is a BlueprintNativeEvent)




};
