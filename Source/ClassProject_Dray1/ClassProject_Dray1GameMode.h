// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SpawnVolume.h"//needed to call our spawnvolume
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClassProject_Dray1GameMode.generated.h"




enum class EClassProject_Dray1PlayState : short
{
	EPlaying,//for when the game is active
	EGameOver,//for when the game ends
	EUnknown//for if there's an error
};


UCLASS()
class CLASSPROJECT_DRAY1_API AClassProject_Dray1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	virtual void Tick(float DeltaSeconds) override;

	AClassProject_Dray1GameMode();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Power)//used to work with blueprints in unreal
		float DecayRate;//for the power level to drop

	EClassProject_Dray1PlayState GetCurrentState() const;//gets the game state

	void SetCurrentState(EClassProject_Dray1PlayState NewState);//sets new game state

	virtual void BeginPlay() override;


private:

	TArray<ASpawnVolume*> SpawnVolumeActors;//array for spawn volume

	EClassProject_Dray1PlayState CurrentState;

	void HandleNewState(EClassProject_Dray1PlayState NewState);

};





FORCEINLINE EClassProject_Dray1PlayState AClassProject_Dray1GameMode::GetCurrentState() const
{
	return CurrentState;
}
