// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"

/**
 *
 */
UCLASS()
class CLASSPROJECT_DRAY1_API AMyHUD : public AHUD
{
	GENERATED_BODY()

		AMyHUD();// Sets default values for this actor's properties


	UPROPERTY(EditAnywhere, Category = HUDFont)
		UFont* HUDFont;//stores the font


	virtual void DrawHUD() override;//draw call for the HUD



};

