// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "WHead.generated.h"

UCLASS()
class CLASSPROJECT_DRAY1_API AWHead : public AActor

{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWHead();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Parameters")
		FVector CubeRaduis = FVector(18.f, 18.f, 18.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Point Parameters")
		FVector HoleRaduis = FVector(12.f, 12.f, 12.f);

protected:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* ThisScene;
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisMesh;

	virtual void PostActorCreated() override;
	virtual void PostLoad() override;

	void GenerateMesh();

private:
	TArray<FVector>Vertices;
	TArray<int32>Triangles;
	TArray<FVector>Normals;
	TArray<FProcMeshTangent>Tangents;
	TArray<FVector2D>UVs;
	TArray<FLinearColor>Colors;

	void AddTriangleMesh(FVector TopRight, FVector BottomRight, FVector BottomLeft, int32& TriIndex, FProcMeshTangent Tangent);
	void AddQuadMesh(FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32& TriIndex, FProcMeshTangent Tangent);


};
