// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include"Components/BoxComponent.h"//used to make box spawner
#include "Runtime/Engine/Classes/Engine/World.h"//used for world settings
#include "Pickup.h"//calls on puckup we made
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnVolume.generated.h"

UCLASS()
class CLASSPROJECT_DRAY1_API ASpawnVolume : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawnVolume();

	UPROPERTY(VisibleInstanceOnly, Category = Spawning)//used to work with blueprints in unreal
		UBoxComponent* WhereToSpawn;	//BoxComponent to for the spawning area


	UPROPERTY(EditAnywhere, Category = Spawning)//used to work with blueprints in unreal
		TSubclassOf<class APickup> WhatToSpawn;	//The pickup to spawn 


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)//used to work with blueprints in unreal
		float SpawnDelayRangeLow;	// Minimum spawn delay


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)//used to work with blueprints in unreal
		float SpawnDelayRangeHigh;	//Maximum spawn delay


	UFUNCTION(BlueprintPure, Category = Spawning)//used to work with blueprints in unreal
		FVector GetRandomPointInVolume();	//randomize spawn in BoxComponent


	virtual void Tick(float DeltaSeconds) override;

	void EnableSpawning();//turns on spawning

	void DisableSpawning();//turns off spawning


private:

	bool bSpawningEnabled;	// turns on and off spawning


	float GetRandomSpawnDelay();	// Calculates the random spawn delay


	float SpawnDelay;	//spawn delay


	void SpawnPickup();	// Handles spawning of a new pickup


	float SpawnTime;	// timer for when to spawn the pickup






};
