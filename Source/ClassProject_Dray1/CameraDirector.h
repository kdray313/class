// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraDirector.generated.h"

UCLASS()
class CLASSPROJECT_DRAY1_API ACameraDirector : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACameraDirector();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	const float SmoothBlendTime = 0.5f;//for the tarnaision between cameras
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	APlayerController* OurPlayerController;//creats OurPlayerContriller to set the view

	UPROPERTY(EditAnywhere)//Indicates Cam1 can be edited via property windows, archetypes and instances within the Editor
		AActor* Cam1;

	UPROPERTY(EditAnywhere)//Indicates Cam2 can be edited via property windows, archetypes and instances within the Editor
		AActor* Cam2;

	UPROPERTY(EditAnywhere)//Indicates CamF can be edited via property windows, archetypes and instances within the Editor
		AActor* CamF;

	void SetCam1();//Declares function for changing camera when pressing 1 
	void SetCam2();//Declares function for changing camera when pressing 2
	void SetCamF();//Declares function for changing camera when pressing 0 


};

