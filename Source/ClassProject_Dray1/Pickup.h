// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"//used for meshs
#include "Components/SphereComponent.h"//used for spheres
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS()
class CLASSPROJECT_DRAY1_API APickup : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickup();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickup)//used to work with blueprints in unreal
		bool bIsActive;// when the pickup can or can't be picked up

		//collision primitive to use as the root component
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Pickup)//used to work with blueprints in unreal
		USphereComponent* BaseCollisionComponent;//collision primitive to use as the root component

		/** StaticMeshComponent to represent the pickup in the level */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pickup)//used to work with blueprints in unreal
		UStaticMeshComponent* PickupMesh;//Mesh to represent the pickup


		/** Function to call when the Pickup is collected */
	UFUNCTION(BlueprintNativeEvent)
		void OnPickedUp();// Function for when the Pickup is collected



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;



};
