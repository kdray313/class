// Fill out your copyright notice in the Description page of Project Settings.



#include "ComponentOverlap.h"
#include "ClassProject_Dray1.h"



// Sets default values
AComponentOverlap::AComponentOverlap(const FObjectInitializer& objectInitializer)
	:Super(objectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_collision = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));//defines _collision as sphere component

	_collision->SetSphereRadius(250.f);//size of the sphere
	_collision->OnComponentBeginOverlap.AddDynamic(this, &AComponentOverlap::OnComponentOverlap);//sets the overlap with player

	RootComponent = _collision;// sets RootComponent to _collision 
}

// Called when the game starts or when spawned
void AComponentOverlap::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AComponentOverlap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AComponentOverlap::OnComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)// function that prints text
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Don't Touch ME!!!");// what the text looks like
}