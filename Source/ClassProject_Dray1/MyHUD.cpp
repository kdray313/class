// Fill out your copyright notice in the Description page of Project Settings.

#include "MyHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Font.h"
#include "ClassProject_Dray1GameMode.h"//comunicates with my game
#include "MyPlayer.h"//comunicates with my payer
#include "Kismet/GameplayStatics.h"


AMyHUD::AMyHUD()
{
	ConstructorHelpers::FObjectFinder<UFont>HUDFontOb(TEXT("/Engine/EngineFonts/RobotoDistanceField"));

	HUDFont = HUDFontOb.Object;//Uses font from the engine
}



void AMyHUD::DrawHUD()
{

	FVector2D ScreenDimensions = FVector2D(Canvas->SizeX, Canvas->SizeY);//screen dimensions


	Super::DrawHUD();//parent versions of DrawHUD


	AMyPlayer* MyPlayer = Cast<AMyPlayer>(UGameplayStatics::GetPlayerPawn(this, 0));
	FString PowerLevelString = FString::Printf(TEXT("%10.1f"), FMath::Abs(MyPlayer->PowerLevel));
	DrawText(PowerLevelString, FColor::White, 500, 50, HUDFont);//Get the character and print its power level

	AClassProject_Dray1GameMode* MyGameMode = Cast<AClassProject_Dray1GameMode>(UGameplayStatics::GetGameMode(this));

	if (MyGameMode->GetCurrentState() == EClassProject_Dray1PlayState::EGameOver)//when game is over
	{
		FVector2D GameOverSize;
		GetTextSize(TEXT("GAME OVER"), GameOverSize.X, GameOverSize.Y, HUDFont);// makes the size for printing Game Over

		DrawText(TEXT("GAME OVER"), FColor::Red, (ScreenDimensions.X - GameOverSize.X) / 2.0f, (ScreenDimensions.Y - GameOverSize.Y) / 2.0f, HUDFont);// printing Game Over

	}



}
